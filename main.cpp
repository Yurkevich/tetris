#include <iostream>
#include <conio.h>
#include <chrono>
#include <thread>
#include <windows.h>
#include <math.h>
#include <ctype.h>

#include "field.h"
#include "ladder.h"

using namespace std;

int scores = 0;
string userName = "Unknown";

const int ESC=27;
const int ENT=13;
const int key_s = 115;
const int key_w = 119;
const int key_a = 97;
const int key_d = 100;
const int key_e = 101;

void drawField(Field* field){
    system("cls");
    cout << userName << endl << endl;
    for (int i = 0; i < field->width; i++) {
        cout << "_";
    }
    cout << "   scores: " << scores << endl;
    for (int i = 0; i < field->height; i++) {
        cout << "*";
        for (int j = 0; j < field->width; j++) {
            if (field->field[i][j])
                cout << "#";
            else
                cout << " ";
        }
        cout << "*" << endl;
    }
    for (int i = 0; i < field->width; i++) {
        cout << "_";
    }
}

int actTime;

void mainLoop(int x, int y) {
    Field field(&scores, x, y);
    Figure* f = new Figure(&field, rand() % 7);
    field.addFigure(f);
    // init
    actTime = 500;
    int keyPressed = -1;
    int curActTime;
    // ----
    drawField(&field);

    while (true) {
        curActTime = 0;
        while (curActTime < actTime) {
            if (_kbhit()) {
                keyPressed = _getch();
                // do actions;
                switch (keyPressed) {
                case key_a:
                    field.removeFigure(f);
                    f->moveLeft();
                    field.addFigure(f);
                    break;
                case key_d:
                    field.removeFigure(f);
                    f->moveRight();
                    field.addFigure(f);
                    break;
                case key_e:
                    field.removeFigure(f);
                    f->rotate();
                    field.addFigure(f);
                    break;
                case key_s:
                    field.removeFigure(f);
                    f->moveDown();
                    // do COLLIDE FUNC!!!!!!
                    if (field.isCollide(f->elements)) {
                        f->moveUp();
                        field.addFigure(f);
                        field.checkFieldForDrops();
                        delete f;
                        f = new Figure(&field, rand() % 7);
                        if (field.isCollide(f->elements)) {
                            cout << endl << "GAME OVER";
                            _getch();
                            return;
                        }
                    }
                    // ----------------
                    field.addFigure(f);
                    drawField(&field);
                    continue;
                    break;
                case ESC:
                    return;
                default:
                    break;
                }
                keyPressed = -1;
                drawField(&field);
            }
            curActTime++;
            this_thread::sleep_for(chrono::milliseconds(1));
        }
        field.removeFigure(f);
        f->moveDown();
        // do COLLIDE FUNC!!!!!!
        if (field.isCollide(f->elements)) {
            f->moveUp();
            field.addFigure(f);
            field.checkFieldForDrops();
            delete f;
            f = new Figure(&field, rand() % 7);
            if (field.isCollide(f->elements)) {
                cout << endl << "GAME OVER";
                _getch();
                return;
            }
        }
        // ---------------------
        field.addFigure(f);
        drawField(&field);
    }
}

bool menuItem = false;
void drawMenuScreen() {
    system("cls");
    cout << "MAIN MENU" << endl;
    if (!menuItem) {
        cout << "**  PLAY  **" << endl;
        cout << "   Ladder   " << endl;
    } else {
        cout << "    PLAY    " << endl;
        cout << "** Ladder **" << endl;
    }
}

int main()
{
    Ladder ladder;
    ladder.readladder();
    bool exit = false;
    while (!exit) {
        drawMenuScreen();
        if (_kbhit()) {
            int keyPressed = _getch();
            // do actions;
            switch (keyPressed) {
            case key_w:
            case key_s:
                menuItem = !menuItem;
                break;
            case ENT:
                if (!menuItem) {
                    system("cls");
                    cout << "enter your name: ";
                    cin >> userName;
                    char *temp = new char[1];
                    bool isOk = true;
                    int width, height;
                    while (true) {
                        cout << endl << "enter field width: ";
                        cin >> temp;
                        for (int i = 0; i < strlen(temp); i++) {
                            cout << isdigit(temp[i]);
                            if (!isdigit(temp[i])) {
                                isOk = false;
                                break;
                            }
                        }
                        if (!isOk) continue;
                        width = atoi(temp);
                        break;
                    }
                    isOk = true;
                    while (true) {
                        cout << endl << "enter field height: ";
                        cin >> temp;

                        for (int i = 0; i < strlen(temp); i++) {
                            if (!isdigit(temp[i])){
                                isOk = false;
                                break;
                            }
                        }
                        if (!isOk) continue;
                        height = atoi(temp);
                        break;
                    }
                    delete[] temp;
                    mainLoop(width, height);
                    ladder.addLine(scores, userName);
                    ladder.writeLadder();
                } else {
                    ladder.showladder();
                    _getch();
                }
                break;
            case ESC:
                return 0;
            default:
                break;
            }
        }
        this_thread::sleep_for(chrono::milliseconds(100));
    }
    return 0;
}

