#ifndef FIGURE_H
#define FIGURE_H

class Field;

struct Point
{
    Point(int x, int y);
    Point();
    int x, y;
    Point invert();
};

Point operator+(const Point &l, const Point &r);
Point operator-(const Point &l, const Point &r);

class Figure
{
public:
    Figure(Field *field, int type);
    ~Figure() = default;
    enum Shape {
        RectT = 0,
        RightGT,
        LeftGT,
        RightZT,
        LeftZT,
        TT,
        StikT
    };
    int type;

    Point elements[4];

    Field *field;
    void moveDown();
    void moveUp();
    void moveRight();
    void moveLeft();
    void rotate();

private:
    void setShape();
};

#endif // FIGURE_H
