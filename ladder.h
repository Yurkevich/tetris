#ifndef LADDER_H
#define LADDER_H

#include <iostream>
#include <fstream>
#include <list>

using namespace std;

struct LadderLine
{
    LadderLine(int scores,
               string userName);
    LadderLine();
    int scores;
    string userName;
};

bool operator <(const LadderLine &l, const LadderLine &r);
bool operator >(const LadderLine &l, const LadderLine &r);

class Ladder
{
public:
    Ladder();

    list<LadderLine> ladder;
    void writeLadder();
    void readladder();
    void addLine(int score, string userName);
    void showladder();
};

#endif // LADDER_H
