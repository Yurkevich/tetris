#include "field.h"
#include <iostream>

Field::Field(int *scores)
{
    *(this) = Field(scores, 20, 20);
}

Field::~Field()
{
    for (int i = 0; i < height; i++) {
        delete[] field[i];
    }
    delete[] field;
}

Field::Field(int *scores, int width, int height)
{
    this->scores = scores;
    this->width = width;
    this->height = height;
    field = new bool*[height];
    for (int i = 0; i < height; i++) {
        field[i] = new bool[width];
        for (int j = 0; j < width; j++) {
            field[i][j] = false;
        }
    }
}

void Field::addFigure(Figure* fig)
{
    addOrRemoveFigure(fig, true);
}

void Field::removeFigure(Figure *fig)
{
    addOrRemoveFigure(fig, false);
}

bool Field::isCollide(Point *el)
{
    for (int i = 0; i < 4; i++) {
        if (el[i].x >= height ||
            field[ el[i].x ][ el[i].y ] == true) {
            return true;
        }
    }
    return false;
}

void Field::checkRowForDrop(int row)
{
    for (int i = 0; i < width; i++) {
        if (!field[row][i]) return;
    }
    //add scores
    *scores = *scores + 1;
    //rebuild field;
    for (int i = row; i > 0; i--) {
        field[i] = field[i-1];
    }
    field[0] = new bool[width];
    for (int i = 0; i < width; i++) {
        field[0][i] = false;
    }
    checkRowForDrop(row);

}

void Field::checkFieldForDrops()
{
    for (int i = height - 1; i != 0; i--) {
        checkRowForDrop(i);
    }
}

void Field::addOrRemoveFigure(Figure *fig, bool act)
{
    for (int i = 0; i < 4; i++) {
        field[fig->elements[i].x]
             [fig->elements[i].y]
                = act;
    }
}

