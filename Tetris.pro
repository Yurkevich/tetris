TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    figure.cpp \
    field.cpp \
    ladder.cpp

HEADERS += \
    figure.h \
    field.h \
    ladder.h
LIBS += -L$(FTDI) -lftd2xx
#LIBS += -L"F:\Workspace\qt\libs\ftdi\amd64" -lftd2xx.lib
