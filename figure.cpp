#include "figure.h"
#include "field.h"
#include <algorithm>

Figure::Figure(Field *field, int type)
{
    this->field = field;
    this->type = type;
    setShape();
}

void Figure::moveDown()
{
    for (int i = 0; i < 4; i++) {
        elements[i].x++;
    }
}

void Figure::moveUp()
{
    for (int i = 0; i < 4; i++) {
        elements[i].x--;
    }
}

void Figure::moveRight()
{
    Point temp[4] = elements;
    for (int i = 0; i < 4; i++) {
        temp[i].y++;
        if (temp[i].y > field->width - 1) return;
    }
    if (field->isCollide(temp)) return;
    std::copy(std::begin(temp), std::end(temp), std::begin(elements));
}

void Figure::moveLeft()
{
    Point temp[4] = elements;
    for (int i = 0; i < 4; i++) {
        temp[i].y--;
        if (temp[i].y < 0) return;
    }
    if (field->isCollide(temp)) return;
    std::copy(std::begin(temp), std::end(temp), std::begin(elements));
}

void Figure::rotate()
{
    Point temps[4] = elements;
    for (int i = 0; i < 4; i++) {
        Point temp = elements[1] - temps[i];
        temp = temp.invert();
        temps[i] = temp + elements[1];
        if (temps[i].x < 0 || temps[i].y < 0
         || temps[i].x > field->height - 1 || temps[i].y > field->width - 1)
            return;
    }
    std::copy(std::begin(temps), std::end(temps), std::begin(elements));
}

void Figure::setShape()
{
    switch (type) {
    case RectT:
        elements[0] = Point(0,0);
        elements[1] = Point(0,1);
        elements[2] = Point(1,0);
        elements[3] = Point(1,1);
        break;
    case RightGT:
        elements[0] = Point(0,2);
        elements[1] = Point(1,2);
        elements[2] = Point(1,1);
        elements[3] = Point(1,0);
        break;
    case LeftGT:
        elements[0] = Point(0,0);
        elements[1] = Point(1,0);
        elements[2] = Point(1,1);
        elements[3] = Point(1,2);
        break;
    case RightZT:
        elements[0] = Point(0,2);
        elements[1] = Point(0,1);
        elements[2] = Point(1,1);
        elements[3] = Point(1,0);
        break;
    case LeftZT:
        elements[0] = Point(0,0);
        elements[1] = Point(0,1);
        elements[2] = Point(1,1);
        elements[3] = Point(1,2);
        break;
    case TT:
        elements[0] = Point(0,0);
        elements[1] = Point(0,1);
        elements[2] = Point(0,2);
        elements[3] = Point(1,1);
        break;
    case StikT:
        elements[0] = Point(0,0);
        elements[1] = Point(0,1);
        elements[2] = Point(0,2);
        elements[3] = Point(0,3);
        break;
    default:
        break;
    }
}

Point::Point(int x, int y)
{
    this->x = x;
    this->y = y;
}

Point::Point()
{
    x = 0;
    y = 0;
}

Point Point::invert()
{
    return Point(-y, x);
}

Point operator+(const Point &l, const Point &r)
{
    return Point(l.x + r.x, l.y + r.y);
}

Point operator-(const Point &l, const Point &r)
{
    return Point(l.x - r.x, l.y - r.y);
}
