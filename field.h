#ifndef FIELD_H
#define FIELD_H
#include "figure.h"

class Field
{
public:
    Field(int *scores, int width, int height);
    Field(int *scores);
    ~Field();

    int width;
    int height;

    int *scores;
    bool **field;
//    bool field[20][20];
    void addFigure(Figure *fig);
    void removeFigure(Figure *fig);
    bool isCollide(Point *el);
    void checkFieldForDrops();
private:
    void addOrRemoveFigure(Figure *fig, bool act);
    void checkRowForDrop(int row);
};

#endif // FIELD_H
