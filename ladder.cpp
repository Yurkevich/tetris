#include "ladder.h"
#include <string.h>

Ladder::Ladder()
{
}

void Ladder::writeLadder()
{
    remove( "ladder" );
    ofstream file;
    file.open("ladder", std::ios::binary);
    for (list<LadderLine>::iterator iter = ladder.begin();
         iter != ladder.end(); iter++)
    {
        file << (*iter).scores
             << (*iter).userName << " ";
    }
    file.close();
}

void Ladder::readladder()
{
    ladder.clear();
    ifstream file;
    if(!ifstream("ladder")) return;
    file.open("ladder", std::ios::binary);
    while (!file.eof()) {
        LadderLine l;
        file >> l.scores;
        if (file.eof()) break;
        file >> l.userName;
        ladder.push_back(l);
    }
    ladder.sort();
    ladder.reverse();
}

void Ladder::addLine(int score, string userName)
{
    ladder.push_back(LadderLine(score, userName));
    ladder.sort();
    ladder.reverse();
}

void Ladder::showladder()
{
    system("cls");
    int pos = 1;
    for (list<LadderLine>::iterator iter = ladder.begin();
         iter != ladder.end(); iter++)
    {
        cout << pos
             << " "
             << (*iter).userName
             << " "
             << (*iter).scores
             << endl;
        pos++;
    }
}


bool operator <(const LadderLine &l, const LadderLine &r)
{
    if (l.scores < r.scores) return true;
    return false;
}

bool operator >(const LadderLine &l, const LadderLine &r)
{
    if (l.scores > r.scores) return true;
    return false;
}

LadderLine::LadderLine(int scores,
                       string userName)
{
    this->scores = scores;
    this->userName = userName;
}

LadderLine::LadderLine()
{}
